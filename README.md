## Introduction

Scripts and software connected to the article

Heather M Meyer, José Teles, Pau Formosa-Jordan, Yassin Refahi, Rita San-Bento, 
Gwyneth Ingram, Henrik Jönsson, James C W Locke, Adrienne H K Roeder (2017)
Fluctuations of the transcription factor ATML1 generate the pattern of giant cells in the Arabidopsis sepal
eLife 2017;6:e19131 DOI: 10.7554/eLife.19131

## Files


Currently, the files are accessable via the 
[eLife article page](https://elifesciences.org/articles/19131/figures)

### Supplementary file 1
A zip file containing both Raw data and selected lineages for pATML1::mCitrine-ATML1 (mCitrine-ATML1), 
PDF1::GFP-ATML1 (PDF1), and lgo-2;pATML1::mCitrine-ATML1 (lgo) flowers.

See readme files within the different folders for further information. All raw image 
confocal tif files and example image processing files may be downloaded from: http://dx.doi.org/10.7946/P29G6M

https://doi.org/10.7554/eLife.19131.046



### Source code 1
MATLAB code for all image quantification and analysis, as well as receiver operator 
characteristics (ROC) analysis as described in the Materials and methods section.

https://doi.org/10.7554/eLife.19131.047

### Source code 2
Code for simulating ATML1 dynamics in a growing tissue.

Scripts for the analysis and representation of the simulation results are also provided. 
See readme files within the different folders for further information.

https://doi.org/10.7554/eLife.19131.048


## Additional data

Connected data is also available via DOI 10.7946/P29G6M

Fluctuations of the transcription factor ATML1 generates the pattern of giant cells in the Arabidopsis sepal
Meyer HM Teles J Formosa-Jordan P Refai Y San-Bento R Ingram G Jönsson H Locke JCW Roeder AHK (2016)
Publicly available at Cyverse (http://www.cyverse.org/).

## Contact

henrik.jonsson@slcu.cam.ac.uk

pau.formosa-jordan@slcu.cam.ac.uk
